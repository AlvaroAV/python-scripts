# SELENIUM Examples

1. Start Chrome with Selenium *([01_selenium_chrome.py](01_selenium_chrome.py))*
2. Start Firefox with Selenium *([02_selenium_firefox.py](02_selenium_firefox.py))*
3. Use proxy with Chrome *([03_selenium_proxy_chrome.py](03_selenium_proxy_chrome.py))*
4. Use proxy with Firefox *([04_selenium_proxy_firefox.py](04_selenium_proxy_firefox.py))*
