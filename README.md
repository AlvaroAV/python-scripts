# README #


### What is this repository for? ###

* This repository contains small Python scripts to achieve different stuff. The repository has an Index to maintain structure.
* 0.1.2

# Selenium Examples

1. Start Selenium browser with Chrome
2. Start Selenium browser with Firefox
3. Use proxy with Chrome
4. Use proxy with Firefox

# Urllib Examples

1. Get HTML source code from URL using urllib2 
2. Add headers to urllib2 and test it 
3. Example using Web IP Camera to generate video with urllib 